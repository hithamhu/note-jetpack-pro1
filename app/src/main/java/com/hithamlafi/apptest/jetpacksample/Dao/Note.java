package com.hithamlafi.apptest.jetpacksample.Dao;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

//this annotation to reference this class is pojo
@Entity
public class Note {
@PrimaryKey(autoGenerate = true)
    int nId;
@ColumnInfo(name = "title")//this change column in db from nTitle to title **if not added take the name only
    String nTitle;
    String nBody;



    public Note(String nTitle, String nBody) {
        this.nTitle = nTitle;
        this.nBody = nBody;
    }

    public int getnId() {
        return nId;
    }

    public void setnId(int nId) {
        this.nId = nId;
    }

    public String getnTitle() {
        return nTitle;
    }

    public void setnTitle(String nTitle) {
        this.nTitle = nTitle;
    }

    public String getnBody() {
        return nBody;
    }

    public void setnBody(String nBody) {
        this.nBody = nBody;
    }
}
