package com.hithamlafi.apptest.jetpacksample.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.hithamlafi.apptest.jetpacksample.R;
import com.hithamlafi.apptest.jetpacksample.Dao.Note;

import java.util.List;

public class NoteAdapter extends

        RecyclerView.Adapter<NoteAdapter.ViewHolder> {

    private static final String TAG = NoteAdapter.class.getSimpleName();

    private Context context;

    private List<Note> list;

    private OnItemClickListener onItemClickListener;

    public NoteAdapter(Context context, List<Note> list,

                       OnItemClickListener onItemClickListener) {

        this.context = context;

        this.list = list;

        this.onItemClickListener = onItemClickListener;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,body;
        public ViewHolder(View itemView) {

            super(itemView);
            title=itemView.findViewById(R.id.title);
            body=itemView.findViewById(R.id.body);

        }

        public void bind(final Note model,

                         final OnItemClickListener listener) {
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    listener.onItemClick(getLayoutPosition());
                    return false;
                }
            });



        }

    }

    @Override

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Context context = parent.getContext();

        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.recycleview_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;

    }

    @Override

    public void onBindViewHolder(ViewHolder holder, int position) {

        Note item = list.get(position);

        holder.bind(item, onItemClickListener);
        holder.title.setText(item.getnTitle());
        holder.body.setText(item.getnBody());

    }

    @Override

    public int getItemCount() {

        return list.size();

    }

    public interface OnItemClickListener {

        void onItemClick(int position);

    }

}