package com.hithamlafi.apptest.jetpacksample.ui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hithamlafi.apptest.jetpacksample.R;
import com.hithamlafi.apptest.jetpacksample.Dao.Note;
import com.hithamlafi.apptest.jetpacksample.repository.RepositoryImpl;

public class AddNote extends AppCompatActivity implements View.OnClickListener {

    private EditText title;
    private EditText body;
    private FloatingActionButton fab;

    private final String TAG = this.getClass().getName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        Log.e(TAG,"_ActivityInit");

        // init vars
        title =  findViewById(R.id.title);
        body = findViewById(R.id.body);
        fab =  findViewById(R.id.add_btn);

        // set listener to FAB
        fab.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.e(TAG, "_onClickInvoked");
        RepositoryImpl.getInstance().insertNote(new Note(title.getText().toString(), body.getText().toString()));
        // show toast
        Toast.makeText(AddNote.this,"Record Added", Toast.LENGTH_SHORT).show();
        // get back to main activity
        finish();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
