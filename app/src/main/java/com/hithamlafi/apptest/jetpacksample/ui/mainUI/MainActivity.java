package com.hithamlafi.apptest.jetpacksample.ui.mainUI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hithamlafi.apptest.jetpacksample.R;
import com.hithamlafi.apptest.jetpacksample.adapter.NoteAdapter;
import com.hithamlafi.apptest.jetpacksample.Dao.Note;
import com.hithamlafi.apptest.jetpacksample.repository.RepositoryImpl;
import com.hithamlafi.apptest.jetpacksample.ui.AddNote;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    RecyclerView recyclerView;
    FloatingActionButton addNoteBtn;
    NoteAdapter adapter;
    MainActivityViewModel activityViewModel;
    List<Note> noteList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityViewModel= ViewModelProviders.of(this).get(MainActivityViewModel.class);
        initUI();

        addNoteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AddNote.class));
            }
        });

    }

    private void initUI(){
        recyclerView=findViewById(R.id.recyclerview);
        addNoteBtn=findViewById(R.id.addNote_btn);
        noteList=new ArrayList<>();
        adapter=new NoteAdapter(this, noteList, new NoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Note note=noteList.get(position);
                RepositoryImpl.getInstance().deleteNote(note);
                adapter.notifyDataSetChanged();
                noteList.remove(position);
                Toast.makeText(MainActivity.this, "Record removed", Toast.LENGTH_SHORT).show();

            }
        });
        activityViewModel.getNoteList().observe(this, new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {
                // update DataSet
                Log.e(TAG,"_AdapterIsUpdatedFromViewModel");
                noteList.clear();
                noteList.addAll(activityViewModel.getNoteList().getValue());
                adapter.notifyDataSetChanged();
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);




    }

}

