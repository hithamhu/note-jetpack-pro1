package com.hithamlafi.apptest.jetpacksample.repository;

import androidx.lifecycle.LiveData;

import com.hithamlafi.apptest.jetpacksample.Dao.Note;

import java.util.List;

public interface Repository {

    void insertNote(Note note);
    void deleteNote(Note note);
    LiveData<List<Note>>getNotes();
}
