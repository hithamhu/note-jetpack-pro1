package com.hithamlafi.apptest.jetpacksample.repository;

import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.room.Room;

import com.hithamlafi.apptest.jetpacksample.Dao.NoteDatabase;
import com.hithamlafi.apptest.jetpacksample.MyApplication;
import com.hithamlafi.apptest.jetpacksample.Dao.Note;

import java.util.List;

/**
 * this class for link between component and DB
 * الكلاس من نوع Singleton لأن جميع الكلاسات ستتواصل مع Repository واحدة فقط.
 */
public class RepositoryImpl implements Repository{
    private static final String TAG = "RepositoryImpl";
   private static RepositoryImpl repository=new RepositoryImpl();
   private static NoteDatabase noteDatabase;//to connect to database and make operation

    // operations constants
    private static final int INSERT_OPERATION = 0;
    private static final int DELETE_OPERATION = 1;

    public RepositoryImpl() {
        initDB();
    }

    public static Repository getInstance(){
        return repository;//Singleton Object
    }

    private void initDB(){
        Log.e(TAG, "_DataBaseInit");
        noteDatabase= Room.databaseBuilder(MyApplication.getAppContext(),NoteDatabase.class,"NoteDatabase").build();
    }

    @Override
    public void insertNote(Note note) {
        Log.e(TAG, "_noteIsAddedToDB");
        new DatabaseOpertation(note,INSERT_OPERATION).execute() ;
    }

    @Override
    public void deleteNote(Note note) {
        Log.e(TAG, "deleteNote: noteIsDeleted");
        new DatabaseOpertation(note,DELETE_OPERATION).execute();

    }

    @Override
    public LiveData<List<Note>> getNotes() {
//        int size=noteDatabase.noteDao().getAllNotes().getValue().size();
        Log.e(TAG, "getNotes: getAllNote" );
        return noteDatabase.noteDao().getAllNotes();
    }

    /**
     * نقوم بعمل Override لجميع الـ Methods الموجودة داخل الـ Interface. ( ملاحظة :
     * عمليات الإضافة والحذف تتطلب العمل على Background thread
     * وأي محاولة لتنفيذها على الـ main thread سيتم عمل throw لـ Exception ومنعك من قبل Room نفسها. )
     * هناك عدة طرق لحل هذه المشكلة ولكن وجدت أسهلها عمل Inner class يرث Async.

     */


    class DatabaseOpertation extends AsyncTask<Void,Void,Void>{
        public Note note;
        public int operation;

        public DatabaseOpertation(Note note, int operation) {
            this.note = note;
            this.operation = operation;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.e(TAG, "_doInBackgroundInvoked");
            // INSERT operation
            if (operation == INSERT_OPERATION) {
                noteDatabase.noteDao().insertNote(note);
            }else
                // else delete ...
                if (operation==DELETE_OPERATION){
                noteDatabase.noteDao().deleteNote(note);
            }


            return null;
        }
    }
}
