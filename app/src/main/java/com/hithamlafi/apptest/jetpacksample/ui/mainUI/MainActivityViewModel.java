package com.hithamlafi.apptest.jetpacksample.ui.mainUI;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.hithamlafi.apptest.jetpacksample.Dao.Note;
import com.hithamlafi.apptest.jetpacksample.repository.RepositoryImpl;

import java.util.List;

/**
 * كما ذكرنا سابقاً بأن الغرض الرئيسي من الـ ViewModel هو تخزين المعلومات المراد عرضها داخل الـ View وعزلها تماماً حتى لا تتأثر بأي Lifecycle change قد يطرأ على الـ View.
 */

public class MainActivityViewModel extends ViewModel {
    private static final String TAG = "MainActivityViewModel";
    LiveData<List<Note>> noteList;

    public LiveData<List<Note>>getNoteList(){
        if (noteList==null){
            Log.e(TAG, "_ListItemsIsNULL");
            noteList=new MutableLiveData<>();
            loadItemsFromRepository();
        }
        Log.e(TAG, "_ReturningFromViewModel");
        return noteList;
    }




    private void loadItemsFromRepository() {
        Log.e(TAG, "_LoadFromDB");
        noteList = RepositoryImpl.getInstance().getNotes();
    }
}
